@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center mb-2 py-2" style="justify-content: center; gap: 20px;">
    <a class="btn btn-primary" href="/">Back</a>
    <h3>Files</h3>
    <a class="btn btn-primary" href="{{$folders->id}}/file/upload/">Add New File</a>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    {{ __($count) }} 
                    @if ($count == 1)
                    file
                    @else files
                    @endif
                </div>

                
                <ul class="list-group">
                
                <!-- <li class="list-group-item">Cras justo odio</li> -->
                
                @foreach ($files as $file)
                @if (($file->foldername) == $folderName)
                <li class="list-group-item d-flex justify-content-between">
                   <h5>{{ $file->filename }}{{ $file->extension }}</h5>
                    <div class="d-flex" style="gap: 1px;">
                        <a href="/file/{{ $file->id }}/edit/" class="btn btn-primary">Rename</a>
                        <a href="/file/download/{{ $file->id }}/" class="btn btn-primary">Download</a>
                        <form action="/delete/{{ $file->id }}" method="post">
                        @csrf
                        @method('DELETE')
                            <button type="submit"class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                    </li>
                @endif
                @endforeach
                </div>
                </ul>
                
            </div>
        </div>
        
    </div>
</div>
@endsection
