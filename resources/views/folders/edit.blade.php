@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit folder name') }}</div>
                
                <div class="card-body">
                    <form method="post" action="/{{ $folder->id }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="row mb-3">
                            <label for="foldername" class="col-md-4 col-form-label text-md-end">{{ __('Folder Name') }}</label>

                            <div class="col-md-6">
                                <input id="foldername"
                                    placeholder=""
                                    type="text" 
                                    class="form-control @error('foldername') is-invalid @enderror" 
                                    name="foldername" 
                                    value="{{ old('foldername') ?? $newFolderName}}" 
                                    required autocomplete="foldername" autofocus>

                                @error('Folder Name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Rename Folder') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
