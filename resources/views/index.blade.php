@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center mb-2 py-2" style="justify-content: center; gap: 20px;">
        <a class="btn btn-primary float-right" href="/file/upload/">Add New File</a>
        <h3>Files/Folders</h3>
        <a class="btn btn-primary float-right" href="/folder/create/">Add New Folder</a>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    {{ __($count) }} 
                    @if ($count == 1)
                    file
                    @else files
                    @endif
                    
                    /
                    {{ __($folders->count()) }} 
                    @if (($folders->count()) == 1)
                    folder
                    @else folders
                    @endif
                </div>

                
                <ul class="list-group">
                
                <!-- <li class="list-group-item">Cras justo odio</li> -->
                

                @foreach ($folders as $folder)
                    <li class="list-group-item d-flex justify-content-between"><a href="{{$folder->id}}">
                    <h5>{{ $folder->foldername }}</h5>
                    <div class="d-flex" style="gap: 1px;">
                        <a href="/{{$folder->id}}/edit/" class="btn btn-primary">Rename</a>
                        <!-- <a href="/{{$folder->id}}/download//" class="btn btn-primary">Download</a> -->
                        <form action="/folder/delete/{{$folder->id}}" method="post">
                        @csrf
                        @method('DELETE')
                            <button type="submit"class="btn btn-primary btn-danger">Delete</button>
                        </form>
                    </div>
                    </a>    
                    </li>
                @endforeach
                @foreach ($files as $file)
                @if (($file->foldername) =="")
                <li class="list-group-item d-flex justify-content-between">
                   <h5>{{ $file->filename }}{{ $file->extension }}</h5>
                    <div class="d-flex" style="gap: 1px;">
                        <a href="/file/{{ $file->id }}/edit/" class="btn btn-primary">Rename</a>
                        <a href="/file/download/{{ $file->id }}/" class="btn btn-primary">Download</a>
                        <form action="/delete/{{ $file->id }}" method="post">
                        @csrf
                        @method('DELETE')
                            <button type="submit"class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                    </li>
                @endif
                @endforeach
                </div>
                </ul>
                
            </div>
        </div>
        
    </div>
</div>
@endsection
