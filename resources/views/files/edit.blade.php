@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit file name') }}</div>
                
                <div class="card-body">
                    <form method="post" action="/file/{{ $file->id }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="row mb-3">
                            <label for="filename" class="col-md-4 col-form-label text-md-end">{{ __('File Name') }}</label>

                            <div class="col-md-6">
                                <input id="filename"
                                    placeholder=""
                                    type="text" 
                                    class="form-control @error('filename') is-invalid @enderror" 
                                    name="filename" 
                                    value="{{ old('filename') ?? $fileName}}" 
                                    required autocomplete="filename" autofocus>

                                @error('File Name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Rename File') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
