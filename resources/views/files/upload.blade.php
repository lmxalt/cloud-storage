@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Upload file') }}</div>

                <div class="card-body">
                    <form method="post" action="/file" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label for="filename" class="col-md-4 col-form-label text-md-end">{{ __('File Name') }}</label>

                            <div class="col-md-6">
                                <input id="filename"
                                    placeholder=""
                                    type="text" 
                                    class="form-control @error('filename') is-invalid @enderror" 
                                    name="filename" 
                                    value="{{ old('filename') }}" 
                                    required autocomplete="filename" autofocus>

                                @error('File Name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="file" class="col-md-4 col-form-label text-md-end">{{ __('File') }}</label>

                            <div class="col-md-6">
                                <input id="file" type="file" class="form-control-file" name="file" required>

                                @error('file')
                                        <strong>{{ $message }}</strong>            
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Upload File') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
