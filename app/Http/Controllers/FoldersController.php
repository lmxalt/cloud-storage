<?php

namespace App\Http\Controllers;
use App\Models\Folder;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FoldersController extends Controller
{
    public function index($folders)
    {
        $files = File::orderBy('id', 'ASC')->get();
        $folders = Folder::find($folders);
        $folderName = $folders['foldername'];
        $count = 0;
        foreach ($files as $file) {
            if (($file->foldername) == $folderName) {
                $count += 1;
            }
        }
        return view('folders.index', compact('files','folders', 'folderName', 'count'));
    }

    public function upload($folders)
    {
        $folders = Folder::find($folders);
        $folderName = $folders['foldername']; 
        return view('folders.upload', compact('folders'));
    }

    public function store(Request $request, $folders)
    {
        $data = request()->validate([
            'filename'=>'required',
            'file'=>'required|file|max:20000',
        ]);
        $folders = Folder::find($folders);
        $folderName = $folders['foldername'];
        $upload = $request->file('file');
        $extension = $upload->getClientOriginalExtension();
        $onlyExtension=".".$extension;
        $filePath = $upload->store('public/storage/'.$folderName);
        $getFileSize = $upload->getSize();
        $fileSize = number_format($getFileSize / 1048576,2);
       
        $file = File::create([
            'filename' => $data['filename'],
            'filepath' => $filePath,
            'foldername'=> $folderName,
            'extension' => $onlyExtension,
            'filesize' => $fileSize,
        ]);
        return redirect('/'.$folders->id);
    }

    public function edit($folder)
    {
        $folder = Folder::find($folder);
        $newFolderName = $folder['foldername'];
        return view('folders.edit', compact('folder', 'newFolderName'));
    }

    public function update(Request $request, $folder)
    {   
        $data = request()->validate([
            'foldername'=>'required',
        ]);
        $folder = Folder::find($folder);
        $folderName = $folder['foldername'];
        $folderPath = $folder['folderpath'];
        $id = $folder['id'];
        $newFolderName = $request->input('foldername');
        $create = Storage::makeDirectory($folderPath.$newFolderName);
        $file = Storage::files($folderPath.$folderName);
        $oldFolderPath = "public/storage/".$folderName;
        $newFolderPath = "public/storage/".$newFolderName;
        $getFile = File::where('foldername', $folderName)->get();
        $replace = str_replace($oldFolderPath, "", $file);
        for ($n = 0; $n < count($getFile); $n++)
        {
            $move = Storage::move($oldFolderPath.$replace[$n], $newFolderPath.$replace[$n]);
            $folder = File::where('foldername', $folderName)->update([
                'foldername' => $newFolderName,
                'filepath' => $newFolderPath.$replace[$n],
            ]);
        } 
        $folder= Folder::where('id', $id)->update([
            'foldername' => $newFolderName,
        ]);
        $delete = Storage::deleteDirectory($oldFolderPath);
        return redirect('/');
    }

    public function delete($folder)
    {
        $folder = Folder::find($folder);
        $folderName = $folder['foldername'];
        $folderPath= "public/storage/".$folderName;
        $files = File::where('foldername', $folderName)->get();
        $delete = Storage::deleteDirectory($folderPath);
        $folder->delete();
        foreach($files as $file) {
            $file->delete();  
        }
        return redirect('/');
    }
}
