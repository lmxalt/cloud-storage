<?php

namespace App\Http\Controllers;
use App\Models\File;
use App\Models\Folder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $files = File::orderBy('id', 'ASC')->get();
        $count = 0;
        foreach ($files as $file){
            if (($file->foldername) == "") {
                $count += 1;
            }
        }
        $folders = Folder::orderBy('id', 'ASC')->get();
        /* $fileSize = File::size(public_path());
        dd($fileSize); */
        return view('index', compact('files','folders','count'));
    }

    public function upload()
    {
        return view('files.upload');
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'filename'=>'required',
            'file'=>'required|file|max:20000',
        ]);
        $upload = $request->file('file');
        $extension = $upload->getClientOriginalExtension();
        $onlyExtension=".".$extension;
        $filePath = $upload->store('public/storage');
        $getFileSize = $upload->getSize();
        $fileSize = number_format($getFileSize / 1048576,2);
        $folderName = '';
        $file = File::create([
            'filename' => $data['filename'],
            'filepath' => $filePath,
            'foldername'=> $folderName,
            'extension' => $onlyExtension,
            'filesize' => $fileSize,
        ]);
        /* $request->file('file')->getSize(); */
        /* dd($file->foldername);  */
        return redirect('/');
    }

    public function edit($file)
    {
        $file = File::find($file);
        $fileName = $file['filename'];
        return view('files.edit', compact('file','fileName'));
    }

    public function update(Request $request, $file)
    {   
        $data = request()->validate([
            'filename'=>'required',
        ]);
        $input = $request->all();
        $file = File::find($file);
        $file->filename = $input['filename'];
        $file->save([
            'filename' => $data['filename'],
        ]);
        return redirect('/');
    }
    
    public function download($file)
    {
        $download = File::find($file);
        // $download['filename'] = $download['filename'].$download['extension'];
        return Storage::download($download->filepath, $download->filename);
    }

    public function delete($file)
    {
        $delete = File::find($file);
        Storage::delete($delete->filepath);
        $delete->delete();
        return redirect('/');
    }

    public function folderCreate()
    {
        return view('folders.create');
    }

    public function folderStore(Request $request)
    {
        $data = request()->validate([
            'foldername'=>'required',
        ]);
        $folderName = $request->input('foldername');
        $folderPath = 'public/storage/';
        $create = Storage::makeDirectory($folderPath.$folderName);
        $folder = Folder::create([
            'foldername'=>$folderName,
            'folderpath'=>$folderPath,
        ]);
        return redirect('/');
    }
}
