## Cloud-storage

Cloud-storage - прототип сервиса облачного хранилища для файлов и каталога 1-ого уровня вложенности.

---
<img align="center" src="https://media.giphy.com/media/D7nTfDunlYLR03vlqC/giphy.gif" width="80%">
---

## Функционал

- Хранить файлы
- Создавать папки
- Скачивать/переименовывать/удалять файлы
- Переименовывать/удалять папки
