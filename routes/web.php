<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\FilesController::class, 'index']);
Route::get('/file/upload', [App\Http\Controllers\FilesController::class, 'upload'])->name('upload');;
Route::post('/file', [App\Http\Controllers\FilesController::class, 'store'])->name('store');
Route::get('/file/{file}/edit', [App\Http\Controllers\FilesController::class, 'edit']);
Route::patch('/file/{file}', [App\Http\Controllers\FilesController::class, 'update']);
Route::get('file/download/{file}', [App\Http\Controllers\FilesController::class, 'download']);
Route::delete('/delete/{file}', [App\Http\Controllers\FilesController::class, 'delete']);

Route::get('/folder/create', [App\Http\Controllers\FilesController::class, 'folderCreate']);
Route::post('/folder', [App\Http\Controllers\FilesController::class, 'folderStore']);

Route::get('/{folder}', [App\Http\Controllers\FoldersController::class, 'index']);
Route::get('/{folder}/file/upload', [App\Http\Controllers\FoldersController::class, 'upload']);
Route::post('/{folder}/file', [App\Http\Controllers\FoldersController::class, 'store']);
Route::get('/{folder}/edit', [App\Http\Controllers\FoldersController::class, 'edit']);
Route::patch('/{folder}', [App\Http\Controllers\FoldersController::class, 'update']);
Route::delete('/folder/delete/{folder}', [App\Http\Controllers\FoldersController::class, 'delete']);


